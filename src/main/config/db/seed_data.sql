-- SECURITY SEED DATA --
-- Groups
insert into groups (name) values ('Administrators')
insert into groups (name) values ('Staff')
insert into groups (name) values ('Members')
insert into groups (name) values ('Users')
-- Authorities
insert into authority (authority)  values ('ROLE_ADMINISTRATOR');
insert into authority (authority)  values ('ROLE_STAFF');
insert into authority (authority)  values ('ROLE_MEMBER');
insert into authority (authority)  values ('ROLE_USER');
-- Group Authorities
insert into group_authority (group_id, authority_id) select g.group_id, authority_id from groups g, authority where g.name = 'Administrators' and authority = 'ROLE_ADMINISTRATOR';
insert into group_authority (group_id, authority_id) select g.group_id, authority_id from groups g, authority where g.name = 'Administrators' and authority = 'ROLE_USER';
insert into group_authority (group_id, authority_id) select g.group_id, authority_id from groups g, authority where g.name = 'Staff' and authority = 'ROLE_STAFF';
insert into group_authority (group_id, authority_id) select g.group_id, authority_id from groups g, authority where g.name = 'Staff' and authority = 'ROLE_USER';
insert into group_authority (group_id, authority_id) select g.group_id, authority_id from groups g, authority where g.name = 'Members' and authority = 'ROLE_MEMBER';
insert into group_authority (group_id, authority_id) select g.group_id, authority_id from groups g, authority where g.name = 'Members' and authority = 'ROLE_USER';
insert into group_authority (group_id, authority_id) select g.group_id, authority_id from groups g, authority where g.name = 'Users' and authority = 'ROLE_USER';
-- USERS
insert into users (USERNAME, FIRST_NAME, MIDDLE_NAME, LAST_NAME, PASSWORD, SALT, EMAIL, ACCOUNT_NON_EXPIRED, ACCOUNT_NON_LOCKED, CREDENTIALS_NON_EXPIRED, ENABLED, deleted, FAILED_LOGIN_ATTEMPTS, CREATED_BY, CREATED_ON, OPTLOCK, UPDATED_BY, UPDATED_ON) select 'admin1', 'John', 'D', 'Administrator', 'password', 'salt', 'admin1@example.com', 1, 1, 1, 1, 0, 0, 'SYSTEM', getdate(), 0, null, null;
insert into users (USERNAME, FIRST_NAME, MIDDLE_NAME, LAST_NAME, PASSWORD, SALT, EMAIL, ACCOUNT_NON_EXPIRED, ACCOUNT_NON_LOCKED, CREDENTIALS_NON_EXPIRED, ENABLED, deleted, FAILED_LOGIN_ATTEMPTS, CREATED_BY, CREATED_ON, OPTLOCK, UPDATED_BY, UPDATED_ON) select 'staff1', 'Sally', 'T', 'Staff', 'password', 'salt', 'staff1@example.com', 1, 1, 1, 1, 0, 0, 'SYSTEM', getdate(), 0, null, null;
insert into users (USERNAME, FIRST_NAME, MIDDLE_NAME, LAST_NAME, PASSWORD, SALT, EMAIL, ACCOUNT_NON_EXPIRED, ACCOUNT_NON_LOCKED, CREDENTIALS_NON_EXPIRED, ENABLED, deleted, FAILED_LOGIN_ATTEMPTS, CREATED_BY, CREATED_ON, OPTLOCK, UPDATED_BY, UPDATED_ON) select 'member1', 'Mary', 'N', 'Member', 'password', 'salt', 'member1@example.com', 1, 1, 1, 1, 0, 0, 'SYSTEM', getdate(), 0, null, null;
-- 
insert into group_members select u.users_id, g.group_id from users u, groups g where g.name = 'Administrators' and u.username = 'admin1';
insert into group_members select u.users_id, g.group_id from users u, groups g where g.name = 'Staff' and u.username = 'staff1';
insert into group_members select u.users_id, g.group_id from users u, groups g where g.name = 'Members' and u.username = 'member1';
