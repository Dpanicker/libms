package com.hurontg.libms.mvc;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.ext.XLogger;
import org.slf4j.ext.XLoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.hurontg.libms.domain.Publisher;
import com.hurontg.libms.service.PublisherService;

@Controller
public class PublisherController extends BaseController {

	/**
	 * 
	 */
	private XLogger logger = XLoggerFactory.getXLogger(PublisherController.class
			.getName());

	@Inject
	private PublisherService pubSvc; // Dependency Injection

	/**
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */


	@RequestMapping(value = { "/publishers" }, method = RequestMethod.GET)
	public String showpublishersMainPage(Model model) throws Exception {
		List<Publisher> publishers = pubSvc.findAll();
		model.addAttribute("publisherList", publishers);
		model.addAttribute("title", "This is a cool title");
		return "pubhome";
	}
	
	@RequestMapping(value = { "/publisherEntryForm" }, method = RequestMethod.GET)
	public String showCarEntryForm(Model model) throws Exception {
		return "pubentryform";
	}
	
	
	@RequestMapping(value = { "/saveNewpublishers" }, method = RequestMethod.GET)
	public String saveNewpublisher(@ModelAttribute Publisher publisher,HttpServletRequest request) throws Exception {
		pubSvc.create(publisher);
			return "redirect:/publishers";
	}
	
	@RequestMapping(value = { "/updatepublishers" }, method = RequestMethod.GET)
	public String UpdateNewpublisher (@ModelAttribute Publisher publisher,HttpServletRequest request) throws Exception{
		pubSvc.update(publisher);
		
		return "publisherUpdateForm.jsp";
		
		
	}
	
	@RequestMapping(value = { "/deletepublishers" }, method = RequestMethod.GET)
	public String deleteCar(HttpServletRequest request) throws Exception {
		String id = request.getParameter("publisherId");
		Long publishersId = Long.parseLong(id);
		pubSvc.delete(publishersId);
		return "redirect:/publishers" ;
	}
}
